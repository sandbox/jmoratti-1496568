<?php
/**
 * @file
 * Admin form callbacks for Jewish Calender module.
 */

/**
 * Administration settings form.
 *
 * @see system_settings_form()
 */
function jewish_calender_admin_settings_form($form, &$form_state) {
  $form['jewish_calender_zipcode'] = array(
    '#type' => 'textfield',
    '#title' => t('Zipcode'),
    '#default_value' => variable_get('jewish_calender_zipcode'),
    '#size' => 40,
    '#maxlength' => 128,
    '#required' => TRUE,
    '#description' => t('Please enter the zipcode for where you are displaying for shabbat timing only use US Zipcode.'),
  );
  $form['jewish_calender_havdallah'] = array(
    '#type' => 'textfield',
    '#title' => t('Havdallah'),
    '#default_value' => variable_get('jewish_calender_havdallah'),
    '#size' => 40,
    '#maxlength' => 128,
    '#required' => TRUE,
    '#description' => t('Enter the amount of time your synagague keeps havdallah if you do not want havdallah displayed please enter 0'),
  );
  $form['jewish_calender_translitered'] = array(
    '#type' => 'radios',
    '#title' => t('Hebrew Format'),
    '#default_value' => variable_get('jewish_calender_translitered'),
    '#size' => 50,
    '#options' => array(
      '0' => t('Ashkenazis'),
      '2' => t('Sephardic'),
    ),
    '#description' => t('Enter how would you want the hebrew to be translitered'),
  );
  $form = system_settings_form($form);
  return $form;
}
